return {
    "sindrets/diffview.nvim",
    cmd = { "DiffviewOpen", "DiffviewFileHistory" },
    keys = {
        { "<leader>ho", "<CMD>DiffviewFileHistory<CR>", desc = "File [H]istory [O]pen" },
        { "<leader>hc", "<CMD>DiffviewClose<CR>", desc = "File [H]istory [C]lose" }
    },
    config = function()
        local actions = require("diffview.actions")
    end,
}
