return {
    "kdheepak/lazygit.nvim",
    keys = {
        { "<leader>ga", "<CMD>Git_add<CR>", desc = "[G]it [A]dd" },
        { "<leader>gr", "<CMD>Git_restore<CR>", desc = "Git [R]estore (Revert)" },
        { "<leader>gg", "<CMD>LazyGit<CR>", desc = "Lazy [G]it", noremap = true, silent = true },
        { "<leader>gf", "<CMD>LazyGitFilterCurrentFile<CR>", desc = "[G]it commits in current [F]ile" }
    }
}
