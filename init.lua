-- disable netrw (must be at start of init.lua!)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

--require("keymaps")
--require("lsp")


-- Set to use system clipboard
vim.o.clipboard = "unnamedplus"


-- Set leader key
vim.g.mapleader = " " -- set leader key to space


-- Add lazy manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Load lazy plugins (plugins.bare, plugins.dap)
require("lazy").setup({
    { import = "plugins.bare", cond = function() return not vim.g.vscode end },
    { import = "plugins.dap", cond = function() return not vim.g.vscode end },
}, {
    performance = {
        rtp = {
            disabled_plugins = {
                ""
            },
        },
    },
})


require("setup.colors")
require("setup.git_functions")

-- empty setup using defaults
require("nvim-tree").setup()

vim.cmd [[
    augroup FormatOnSave
        autocmd!
        autocmd BufWritePost *.py lua vim.lsp.buf.format()
    augroup END
]]


-- CONFIG
vim.opt.number = true 		-- Enable line numbers
vim.opt.relativenumber = true 	-- Enable relative line numbers
vim.opt.autoread = true     -- auto read file when changed
vim.opt.splitright = true 	-- vertical split to the right
vim.opt.splitbelow = true 	-- horizontal split to the bottom
vim.opt.termguicolors = true 	-- enable 24-bit RGB colors
vim.opt.updatetime = 300 	-- faster completion
vim.opt.timeoutlen = 500 	-- faster completion

vim.opt.spell = true        -- enable spell check
vim.opt.spelllang = "en"    -- set spell check language to english

vim.opt.showmatch = true 	-- highlight matching [{()}]
vim.opt.list = true         -- show whitespace

vim.opt.ignorecase = true 	-- case insensitive on search
vim.opt.smartcase = true 	-- case sensitive when express in search

vim.opt.hlsearch = true 	-- highlight search results
vim.opt.incsearch = true 	-- incremental search
vim.opt.wrap = false 		-- disable line wrap
vim.opt.cursorline = true 	-- highlight current line

vim.opt.tabstop = 4 		-- tab size
vim.opt.shiftwidth = 4 		-- indent size
--vim.opt.expandtab = true 	-- use spaces instead of tabs
vim.opt.smartindent = true 	-- autoindent new lines
vim.opt.autoindent = true 	-- autoindent new lines

vim.opt.hidden = true 		-- enable background buffers
vim.opt.scrolloff = 8 		-- start scrolling when 8 lines from margins
vim.opt.sidescrolloff = 8 	-- start scrolling when 8 columns from margins


-- Set foldmethod to syntax
vim.opt.foldmethod = "syntax"


-- KEYBINDINGS
vim.keymap.set("n", "<leader>/", ':let @/ = ""<CR>') -- clear search highlight
vim.keymap.set("n", "<leader>n", ":NvimTreeToggle<CR>") -- toggle file explorer
-- Set a keybinding in terminal mode to switch to the window on the left
vim.keymap.set('t', '<leader>w', '<C-\\><C-n>', { noremap = true, silent = true })


-- Lazygit config
vim.g.lazygit_floating_window_winblend = 0
vim.g.lazygit_floating_window_scaling_factor = 0.9
vim.g.lazygit_floating_window_border_chars = {'╭', '─', '╮', '│', '╯', '─', '╰', '│'} vim.g.lazygit_floating_window_use_plenary = 0 vim.g.lazygit_use_neovim_remote = 1 vim.g.lazygit_use_custom_config_file_path = 0
vim.g.lazygit_config_file_path = ''


vim.cmd [[
    let g:ale_pattern_options = {
        \'COMMIT_EDITMSG$': {'ale_linters': [], 'ale_fixers': []}
    \   }
    let g:ale_linters = {
    \   'yaml': ['yamllint'],
    \   'cpp': ['clangtidy'],
    \   'c': ['clangtidy'],
    \   'asciidoc': ['cspell'],
    \   'markdown': ['cspell']
    \   }
    let g:ale_fixers = {
    \   'cpp': ['clang-format'],
    \   'c': ['clang-format']
    \   }

    autocmd FileType c nnoremap <leader>f <plug>(ale_fix)

    " ALE CONFIG
    let g:ale_linters_explicit = 1
    let g:ale_completion_enabled = 1
    let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
    let g:ale_set_balloons=1
    let g:ale_hover_to_floating_preview=1
    let g:ale_use_global_executables = 1
    let g:ale_sign_column_always = 1
    let g:ale_disable_lsp = 1

    " C linting
    let g:ale_c_clangtidy_options = '-checks=-*,cppcoreguidelines-*'
    let g:ale_c_clangtidy_checks = ['readability-*,performance-*,bugprone-*,misc-*']
    let g:ale_c_clangtidy_checks += ['-readability-function-cognitive-complexity']
    let g:ale_c_clangtidy_checks += ['-readability-identifier-length']
    let g:ale_c_clangtidy_checks += ['-misc-redundant-expression']
    let g:ale_c_build_dir_names = ['build', 'release', 'debug']

    " This function searches for the first clang-tidy config in parent directories and sets it
    function! SetClangTidyConfig()
        let l:config_file = findfile('.clang-tidy', expand('%:p:h').';')
        if !empty(l:config_file)
            let g:ale_c_clangtidy_options = '--config=' . l:config_file
            let g:ale_cpp_clangtidy_options = '--config=' . l:config_file
        endif
    endfunction
     
    " Run this for c and c++ files
    autocmd BufRead,BufNewFile *.c,*.cpp,*.h,*.hpp call SetClangTidyConfig()

]]

vim.cmd [[
command! -nargs=0 LazyGit :term lazygit
command! -nargs=0 LazyGitCurrentFile :term lazygit
command! -nargs=0 LazyGitConfig :term lazygit
command! -nargs=0 LazyGitFilter :term lazygit
command! -nargs=0 LazyGitFilterCurrentFile :term lazygit
]]

-- GUTENTAG CONFIG
vim.cmd [[
  let g:gutentags_cache_dir = expand('~/.cache/nvim/tags') " set tags cache directory
  let g:gutentags_generate_on_new = 1
  let g:gutentags_generate_on_missing = 1
  let g:gutentags_generate_on_write = 1
  let g:gutentags_generate_on_empty_buffer = 0
  let g:gutentags_ctags_extra_args = [
      \ '--tag-relative=yes',
      \ '--fields=+ailmnS',
      \ ]
  let g:gutentags_ctags_exclude = [
      \ '*.git', '*.svg', '*.hg',
      \ '*/tests/*',
      \ 'build',
      \ 'dist',
      \ '*sites/*/files/*',
      \ 'bin',
      \ 'node_modules',
      \ 'bower_components',
      \ 'cache',
      \ 'compiled',
      \ 'docs',
      \ 'example',
      \ 'bundle',
      \ 'vendor',
      \ '*.md',
      \ '*-lock.json',
      \ '*.lock',
      \ '*bundle*.js',
      \ '*build*.js',
      \ '.*rc*',
      \ '*.json',
      \ '*.min.*',
      \ '*.map',
      \ '*.bak',
      \ '*.zip',
      \ '*.pyc',
      \ '*.class',
      \ '*.sln',
      \ '*.Master',
      \ '*.csproj',
      \ '*.tmp',
      \ '*.csproj.user',
      \ '*.cache',
      \ '*.pdb',
      \ 'tags*',
      \ 'cscope.*',
      \ '*.css',
      \ '*.less',
      \ '*.scss',
      \ '*.exe', '*.dll',
      \ '*.mp3', '*.ogg', '*.flac',
      \ '*.swp', '*.swo',
      \ '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png',
      \ '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
      \ '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx',
      \ ]
]]

-- GIT BLAME CONFIG
vim.cmd [[
    let g:gitblame_virtual_text_column = 90
]]

-- MINIMAP CONFIG
vim.keymap.set('n', '<leader>mm', ":Minimap<CR>", {})

-- Set fold method to syntax
vim.cmd [[
    set foldmethod=syntax
]]

-- Set opacity same as alacritty
vim.cmd [[
    hi Normal guibg=NONE guifg=NONE
    hi NonText guibg=NONE guifg=NONE
    hi Folded guibg=NONE guifg=#00ffff
    hi CursorLine guibg=#3a3a3a
]]

-- Set color of line number
vim.cmd [[
    hi LineNr guibg=#3a3a3a guifg=#00ffff
    hi CursorLineNr guibg=#3a3a3a guifg=#00ffff
]]

-- Debugging
vim.keymap.set('n', '<F5>', function() require('dap').continue() end)
vim.keymap.set('n', '<F10>', function() require('dap').step_over() end)
vim.keymap.set('n', '<F11>', function() require('dap').step_into() end)
vim.keymap.set('n', '<F12>', function() require('dap').step_out() end)
vim.keymap.set('n', '<Leader>b', function() require('dap').toggle_breakpoint() end)
vim.keymap.set('n', '<Leader>B', function() require('dap').set_breakpoint() end)
vim.keymap.set('n', '<Leader>lp', function() require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end)
vim.keymap.set('n', '<Leader>dr', function() require('dap').repl.open() end)
vim.keymap.set('n', '<Leader>dl', function() require('dap').run_last() end)
vim.keymap.set({'n', 'v'}, '<Leader>dh', function()
  require('dap.ui.widgets').hover()
end)
vim.keymap.set({'n', 'v'}, '<Leader>dp', function()
  require('dap.ui.widgets').preview()
end)
vim.keymap.set('n', '<Leader>df', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.frames)
end)
vim.keymap.set('n', '<Leader>ds', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.scopes)
end)

